object Main {
    def main(args: Array[String]) = {
        val p = 23
        var value : Int = args(0).toInt;
        var counter = 0;
        while (value % p == 0){
            counter += 1;
            value /= p;
        }
        println(s"$p,$counter,Scala,What if Java but Functional?")
    }
}