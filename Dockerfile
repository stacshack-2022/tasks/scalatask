FROM ubuntu:trusty
RUN sudo apt-get -y update
RUN sudo apt-get -y upgrade
RUN sudo apt-get install -y default-jdk scala
RUN mkdir /var/check-23
WORKDIR /var/check-23
COPY ./* ./
CMD [ "./main" ]